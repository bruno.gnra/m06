Consulta el HowTo-PAM l’apartat “7 Exemples PAM: usant el servei session”  
Consulta el man de pam_mkhomedir.so  
Consulta el man de pam_mount.so  
Consulta el man de /etc/security/pam_mount.conf.xml  


Configura el container PAM (--privileged) per provar cada un dels casos següents:  

> Editamos /etc/pam.d/common-session ya que login hace un include de este modulo.  
*pam_mkhomedir.so, para crear el home de los usuarios*  
*pam_mount.so, para montar los volumenes declarados en /etc/security/pam_mount.conf.xml*   

Editamos /etc/pam.d/common-session   
```
session	required	pam_unix.so
session required	pam_mkhomedir.so 
session	required	pam_mount.so 
```

> A tots els usuaris es munta dins el seu home un recurs anomenat tmp de 100M corresponent a un ramdisk tmpfs.

Editamos /etc/security/pam_mount.conf.xml 

```
<!-- Volume definitions -->
<volume
	user="*"
	fstype="tmpfs"
	mountpoint="~/mytmp"
	options="size=100M"
/>
```

> Només a l’usuari unix01 es munta dins el seu home un recurs anomenat tmp de 200M corresponent a un ramdisk tmpfs.

Editamos /etc/security/pam_mount.conf.xml 

```
<!-- Volume definitions -->
<volume
	user="unix01"
	fstype="tmpfs"
	mountpoint="~/tmp"
	options="size=200M"
/>
```

> A l’usuari unix02 se li munta dins el home un recurs NFS de xarxa. *Nota* Creeu un recurs de xarxa NFS per exemple /usr/share/doc exportat per NFS.  

En el host (server):  
```
apt install nfs-server 
systemctl start nfs-server-start    
vim /etc/exports -> /usr/share/doc *(ro,sync)
systemctl restart nfs-server.service 
```

En el docker (client):  
```
apt install nfs-common
service nfs-common start
service rpcbind restart
service nfs-common start
service rpcbind status
```
Editamos /etc/security/pam_mount.conf.xml  
```
<volume
	user="unix02"
	fstype="nfs4"
	server="192.168.0.36"
	path="/usr/share/doc"
	mountpoint="~/mydocs"
/>
```
Pruebas:  
```
su - unix02
df -h -T
mount 
mount | egrep "mydocs|mytmp"
```


