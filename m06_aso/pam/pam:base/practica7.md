Tots els usuaris poden canviar el chfn en l’horari de 8-10  
Cap usuari pot canviar el chfn en l’horari 8-10.  
Tots els usuaris poden canviar el chfn en l’horari de 8-10 i de 16-22.  
L’usuari unix01 pot canviar el finger de 8-14h. Els altres no.  
L'usuari pere només es pot connectar els dies entre setmana (working days)  
L’usuaria marta es pot connectar tots els dies per la tarda  

> */etc/pam.d/chfn*

```
auth		optional	pam_echo.so [ missatge user %u ]
auth		optional	pam_echo.so [ host %h service %s ]
auth		required	pam_unix.so
account		sufficient	pam_time.so 
account 	required	pam_deny.so
```

> */etc/pam.d/login*

```
account		required	pam_time.so 
```
> *Solución*

```
chfn;*;*;Al0800-1000
chfn;*;!*;Al0800-1000
chfn;*;*;Al0800-1000 | Al1600-2200
chfn;*;unix01;Al0800-1400
login;*;pere;Wk
login;*;marta;Al1300-1900
```



