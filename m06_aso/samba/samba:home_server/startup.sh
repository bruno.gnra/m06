#! /bin/bash

#local users

cp ./ldap.conf /etc/ldap/
cp ./nslcd.conf /etc/
cp ./common-session /etc/pam.d/
cp /opt/docker/common-account /etc/pam.d/common-account
cp /opt/docker/common-auth /etc/pam.d/common-auth
cp ./pam_mount.conf.xml /etc/security/
cp ./nsswitch.conf /etc/

users="unix01 unix02 unix03 unix04 unix05"
for user in $users
do 
	useradd -m -s /bin/bash $user
	echo -e "$user\n$user" | passwd $user
done

# engegar serveis per fer el pam_ldap
/usr/sbin/nslcd
/usr/sbin/nscd

# Config shares samba | no cal
#
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.
mkdir /var/lib/samba/privat
cp /opt/docker/*.md /var/lib/samba/privat/.
cp /opt/docker/smb.conf /etc/samba/smb.conf

# crear homes ldapusers  | crea cuenta samba user ldap
bash /opt/docker/ldapusers.sh

# engegar servei ssh
mkdir /run/sshd
/usr/sbin/sshd

# engegar samba
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd -F

