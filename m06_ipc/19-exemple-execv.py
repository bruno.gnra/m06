# /usr/bin/python3
#-*- coding: utf-8-*-
import sys,os, signal 

def myusr1(signum,frame):
    print("Signal handler with singal:", signum)
    print("Hola Radiola")
def myuser2(signum,frame):
    print("Signal handler with signal:",signum)
    print("Adeu Andreu")
    sys.exit(0)

# Programa padre 
print("Hola comenzamos el prorama principal")
print("PID pare:", os.getpid())

# Creamos el proceso hijo 
pid = os.fork()

# Programa padre plega 
if pid != 0:
    print("Programa padre", os.getpid(), pid)
    print("Programa padre finalizado")
    sys.exit(0)

# Programa hijo 
print("Programa Hijo", os.getpid(), pid)

#os.execv("/usr/bin/ls", ["/usr/bin/ls","-la","."]) # cmd + options + path
#os.execl("/usr/bin/ls","/usr/bin/ls","-la",".") # l, hardcode el programa no es dinamico
#os.execlp("ls","ls","-lh","/opt") # aqui no hace falta poner la ruta absoluta del programa
#os.execvp("uname",["uname","-a"])
#os.execv("/bin/bash",["/bin/bash","show.sh"])
os.execle("/bin/bash","/bin/bash","show.sh",{"nom":"joan","edat":"25"})
signal.signal(signal.SIGUSR1,myusr1) # 10 
signal.signal(signal.SIGUSR2,myuser2) # 12 

# while True: 
#     pass 
# esta linea nunca se ejecutara 
print("Hasta luego lucas ")
sys.exit(0)
