# /usr/share/python3
#-*- coding: utf-8-*-
import sys, socket, argparse
from subprocess import Popen,PIPE

parser = argparse.ArgumentParser(description="""Servidor datatime""")
parser.add_argument("-p","--port",type=int, default=50001)
args = parser.parse_args()

HOST = ''
PORT = args.port

# CREAMOS EL SOCKET TCP/IP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

# enlazamos la conexion con el host y puerto 
s.bind((HOST,PORT))
# escuchamos la conexiones 
s.listen(1)
print("Server escuchando por {}:{}".format(HOST,PORT))
print("Esperando por un nuevo cliente ....")
# Por cada cliente
while True:
    # aceptamos la conexion 
    conn, addr = s.accept()
    print("Conexion por {}".format(addr))
    # comando a ejecutar 
    cmd = ["date"]
    # creamos el popen
    pipeData = Popen(cmd,shell=True,stdout=PIPE)
    # recorremos 
    for line in pipeData.stdout:
        conn.sendall(line)
    # cerramos la conexion
    conn.close()
# No acaba nunca, hay que matarlo con kill 
sys.exit(0)
        