# /usr/bin/python3
#-*- coding: utf-8-*-
# 
# @ edt ASIX M06 CURS 2022-2023
# 
#------------------------------------

class UnixUser():
    """Clase UnixUser: /etc/passwd login:passwd:uid:gid:gecos:home:shell"""
    def __init__(self,l,i,g):
        "Costructor objectes UnixUser"
        self.login=l
        self.uid=i
        self.gid=g

    def show(self):
        print(self.login)
        print(self.uid)
        print(self.gid)

    def __str__(self):
       return "%s %d %d" % (self.login,self.uid,self.gid)

print ("Programa")

user1=UnixUser("pere",1000,1000)
user1.show()
print(user1)

user2=UnixUser("pau",1001,1001)
user2.show()
print(user2)

exit(0)
