import socket

# create a TCP/IP socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# bind the socket to a specific address and port
server_address = ('localhost', 8000)
server_socket.bind(server_address)

# start listening for incoming connections
server_socket.listen(1)
print('Listening on {}:{}'.format(*server_address))

# bucle para procesar mas de un cliente
while True:
    # wait for a new client connection
    print('Waiting for a new client connection...')
    conn, client_address = server_socket.accept()
    print('Connected by', client_address)

    # receive data from the client
    data = conn.recv(1024)
    print('Received data from the client:', data)

    # send data to the client
    conn.sendall(b'Hello, client!')

    # close the connection
    conn.close()

