# /usr/bin/python3 
#-*- coding: utf-8-*-
import sys,argparse,socket
from subprocess import Popen,PIPE

parser = argparse.ArgumentParser("""Server daytime""")
parser.add_argument("-s","--server",type=str,default='')
parser.add_argument("-p","--port",type=int,default=50001)
args = parser.parse_args()
# definimos host y puerto
HOST = args.server
PORT = args.port

# creamos socket TCP/IP 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s.setsocckopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

# Iniciamos la conexion 
s.connect((HOST,PORT))
# En este caso el cliente no envia nada
# Esperamos respuesta del servidor
while True: 
    # mientras recibamos datos 
    data = s.recv(1024)
    # en caso de que no 
    if not data:
        # cliente plega
        break
    # recibidos los datos, los mostramos
    print(repr(data))

# cerramos la conexion 
s.close()
# exit 
sys.exit(0)
