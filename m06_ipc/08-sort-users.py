# /usr/bin/python3 
#-*- coding: utf-8-*-
#
# sort-users.py [-s login|gid] file
#
# -----------------------------------------
# edt @ASIX M06 CURS 2022-2023
# Febrer 2023
# -----------------------------------------
import sys, argparse
from functools import cmp_to_key

# definimos los args
parser = argparse.ArgumentParser(description="""Carga una lista de Usuarios UnixUser """)
parser.add_argument("-s","--sort",type=str,dest="criterio",help="Criterio de orden", metavar="sort",choices=["login","gid"],default="login")
parser.add_argument("-f","--file",type=str,dest="file",help="Fichero a procesar",metavar="file",default="/dev/stdin")

args = parser.parse_args()
print(args)

#-----------------------------------------
# creamos la clase 

class UnixUser():
    def __init__(self,userLine):
        userfield = userLine.split(":")
        self.login = userfield[0]
        self.passwd = userfield[1]
        self.uid = int(userfield[2])
        self.gid = int(userfield[3])
    def show(self):
        pass
    def __str__(self):
        return "%s %s %d %d" % (self.login,self.passwd,self.uid,self.gid)
#-----------------------------------------
def sort_login(user):
    return user.login
def sort_gid(user):
    return (user.gid, user.login)


# ----------------------------------------

fileIn = open(args.file,"r")
userList = []
for line in fileIn:
    oneUser = UnixUser(line)
    userList.append(oneUser)
fileIn.close()

if args.criterio == "login":
    userList.sort(key=sort_login)
else:
    userList.sort(key=sort_gid)

#userList = sorted(userList,key=take_camp)

for user in userList:
    print(user)

exit(0)
