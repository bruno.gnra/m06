# /usr/bin/python3
#-*- coding: utf-8-*-
import sys, socket, argparse, signal, os
from subprocess import Popen,PIPE

# definimos args 
parser = argparse.ArgumentParser(description="""Server calendar""")
parser.add_argument("-s","--server",type=str,default='')
parser.add_argument("-p","--port",type=int,default=50001)
parser.add_argument("-a","--any",type=str)
args = parser.parse_args()

# HOST | PUERTO | ANY | PEERS
HOST = args.server
PORT = args.port 
# año 
any = args.any
listapeers = []

# HANDLERS -----------------------------
def myusr1(signum,frame):
    print("Signal handler with signal:",signum)
    print(listapeers)
    sys.exit(0)
def myusr2(signum,frame):
    print("Signal handler with signal:",signum)
    print(len(listapeers))
    sys.exit(0)
def myterm(signum,frame):
    print("Signal handler with signal:",signum)
    print(listapeers,len(listapeers))
    sys.exit(0)

# PROCESO PADRE ------------------------
pid = os.fork()

if pid != 0:
    print("Programa padre: PID {}, {}".format(os.getpid(),pid))
    sys.exit(0)

print("Iniciando daemon cal: PID {},{}".format(os.getpid(),pid))


signal.signal(signal.SIGUSR1,myusr1) # 10 
signal.signal(signal.SIGUSR2,myusr2) # 12
signal.signal(signal.SIGTERM,myterm) # 15
# ---------------------------------------

# CREAMOS SOCKET TPC/IP 
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
# ENLAZAMOS LA CONEXION 
s.bind((HOST,PORT))
# ESCUCHAMOS LAS CONEXIONES 
s.listen(1)

#print("Server escuchando por {}:{}".format(HOST,PORT))
#print("Esperando por un nuevo cliente... ")

# por cada conexion con un cliente
while True: 
    # acepto la conexion 
    conn, addr = s.accept()
    # agramos a la ip a la lista
    listapeers.append(addr)
    print("Conexion por {}".format(addr))
    # comando calendario
    # cmd = ["cal", any]
    cmd = "cal %s" % (any)
    # creamos el popen
    pipeData = Popen(cmd,shell=True, stdout=PIPE)
    # proceso los datos 
    for line in pipeData.stdout:
        # envio la info 
        conn.sendall(line)
    # cerramos la conexion, para recibir una nueva
    conn.close()
# exit 
sys.exit(0)