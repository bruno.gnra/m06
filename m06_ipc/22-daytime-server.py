# /usr/bin/python3
#-*- coding: utf-8-*-
import sys, socket, os
from subprocess import Popen,PIPE 

# definimos host y puerto 
HOST = ''
PORT = 50001

# creamos el socket TCP/IP
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
# eslazamos el socket a la interfaz y puerto indicado 
s.bind((HOST,PORT))
# escuchamos las conexiones 
s.listen(1)
print("Servidor escuchando en {}:{}".format(HOST,PORT))
print("Esperando por un nuevo cliente")

# aceptamos la conexion 
conn, addr = s.accept()
print("Conexion por {}".format(addr))

# comando
cmd = ["date"]
# Recibimos la info del cliente 
# no hay bucle por que solo recibe una peticion 
# Apenas se conecte un cliente, creamos el popen
pipeData = Popen(cmd,shell=True,stdout=PIPE)
#date = print(pideData.stdout.readline(),end="") # mal 
#conn.send(b'date') # otra forma de pasar por bytes
for line in pipeData.stdout:
    # procesamos la info y la enviamos al cliente 
    conn.send(line)
# cierra conexion y plega 
conn.close()
sys.exit(0)

# si el cliente cierra la conexion 
#if not data:
#    break
