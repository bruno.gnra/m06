#/usr/bin/python3
#-*- coding: utf-8-*-
#
# head choices -n 5|10|15 -f fileIn
# 
# -------------------------------
# @edt ASIX M06 Curs 2022-2023
# Febrer 2023
#
import sys, argparse

parser = argparse.ArgumentParser(description="""Mostrar las N primeras lineas""")

parser.add_argument("-n","--nlin", type=int,dest="nlin", help="num de lineas",metavar="num",choices=[5,10,15],default=10)
# positional args
parser.add_argument("file", type=str,help="File a procesar",metavar="file")

args = parser.parse_args()
print(args)


#------------------------------

MAXLINES=args.nlin
# abrimos el fichero
fileIn=open(args.file,"r")
cont = 0 
# por cada linea 
for line in fileIn: 
    # sumamos 1 al contador 
    cont += 1
    # mostramos la linea 
    print(line, end='')
    # si el contador es igual al numero de lineas indicado
    if cont == MAXLINES:
        # corta el bucle 
        break
# cerramos fichero
fileIn.close()
exit(0)
