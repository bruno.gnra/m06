# /usr/bin/python3
#-*- coding: utf-8-*-
#
# head-multi [-n 5|10|15] [-f filein]...
# 
# @ edt ASIX M06 CURS 2022-2023
# --------------------------------------

import sys, argparse


parser = argparse.ArgumentParser(description="""Mostrar las N primeras lineas """) 

parser.add_argument("-n","--nlin",type=int,dest="nlin",help="Numero de lineas", metavar="num",choices=[5,10,15],default=10)
# 
parser.add_argument("-f","--file",type=str,dest="filelist",help="Fichero a procesar",metavar="num",action='append')

parser.add_argument("-v","--verbose",action="store_true",default=False)

args = parser.parse_args()
print(args)
print(args.filelist)

#---------------------------------------

MAXLINES=args.nlin

def headFile(file):
    fileIn = open(file,"r")
    cont = 0 
    for line in fileIn:
        cont += 1 
        print(line, end='')
        if cont == MAXLINES:
            break
    fileIn.close()

for file in args.filelist:
    if args.verbose:
        print("\n",file, 40*"-")
    headFile(file)

exit(0)

    

