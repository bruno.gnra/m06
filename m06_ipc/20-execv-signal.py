# /usr/bin/python3 
import sys, os

# Programa padre
print("Comienza programa padre")
print("PID padre:", os.getpid())

# Creamos el subproceso
pid = os.fork()
if pid != 0: 
    print("Proceso padre", os.getpid(), pid)
    print("Final proceso padre")
    sys.exit(0)

# Programa hijo
print("Programa hijo:", os.getpid(), pid)
# executamos un programa a con os.execv
os.execv("/usr/bin/python3",["/usr/bin/python3","16-signal.py","20"])

sys.exit(0)