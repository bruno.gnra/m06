# /usr/bin/python3 
#-*- coding: utf-8-*-
import sys,socket,argparse
from subprocess import Popen,PIPE

parser = argparse.ArgumentParser(description="""Server daytime one2one""")
parser.add_argument("-s","--server",type=str,default='')
parser.add_argument("-p","--port",type=int,default=50001)
args = parser.parse_args()

HOST = args.server
PORT = args.port 

# CREAMOS SOCKET TCP/IP
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
# INICAMOS LA CONEXION 
s.connect((HOST,PORT))

# ESPERAMOS RESPUESTA DEL SERVER 
while True:
    # mientras recibimos los datos 
    data = s.recv(1024)
    # si no 
    if not data:
        # cierra la conexion
        break 
    # mostramos los datos 
    print("data", repr(data))
# cerramos la conexion
s.close()
# 
sys.exit(0)