# /usr/bin/python
#-*- coding: utf-8-*-
#
#
import argparse 

parser = argparse.ArgumentParser(description='Programa exemple arguments', prog='02-arguments.py')

parser.add_argument("-e","--edat",type=int, dest="userdat",help="edat a processar",metavar="edat")

parser.add_argument("-n","--nom", type=str,help="nom de usuari", metavar="nom") 

args=parser.parse_args()
print(args)
print(args.userdat, args.nom)
