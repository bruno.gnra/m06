# /usr/bin/python3
#-*- coding: utf-8-*-

import sys, os

print("Hola comenzamos el programa principal")
print("PID pare: ", os.getpid())

# creamos un nuevo proceso 
pid = os.fork()

if pid != 0:
    # os.wait permite que el proceso hijo finalize
    #os.wait()
    print("Programa padre: ", os.getpid(), pid)
else: 
    print("Programa hijo", os.getpid(), pid)
    while True: 
        pass

print("Hasta luego lucas")
sys.exit(0)