# /usr/bin/python3
#-*- coding: utf-8-*-
# 
import sys,os,signal

# definimos el handler, funcion que se ejecutara al llamar la señal
def myhandler(signum,frame):
    print("Signal handler with signal:", signum)
    print("hasta luego lucas")
    sys.exit(1)
# 
def mydeath(signum,frame): 
    print("Signal handler with signal:",signum)
    print("no em dona la gana de morir!")

# Assigna un handler a la señal 
signal.signal(signal.SIGALRM,myhandler) # 14
signal.signal(signal.SIGUSR2,myhandler) # 12
signal.signal(signal.SIGUSR1,mydeath)   # 10
# NO MATA AL PROCESO 
signal.signal(signal.SIGTERM,signal.SIG_IGN) # 15
# NO INTERRUMPE  ^+c
signal.signal(signal.SIGINT,signal.SIG_IGN) # 2

signal.alarm(60)
print(os.getpid())
while True:
    pass
signal.alarm(0)
sys.exit(0)