# /usr/bin/python3
#-*- coding: utf-8-*-
# gname-users.py [-s login|gid| gname] -u fileusers -g fileGroup
# 
# -------------------------------------------
# @edt ASIX-M06 CURS 2022-2023
# FEBRERO 2023
# -------------------------------------------
import sys, argparse
from functools import cmp_to_key

# definimos los args 
parser = argparse.ArgumentParser(description="""Listar usuarios UnixUser segun criterio""")
parser.add_argument("-s","--sort",type=str,dest="criterio",help="Criterio a ordenar",metavar="sort",choices=["login","gid","gname"],default="login")
parser.add_argument("-u","--users",type=str,dest="fileU",help="Fichero a procesar",metavar="FileUsers")
parser.add_argument("-g","--group",type=str,dest="fileG",help="FIchero de groups",metavar="FileGrups")
args = parser.parse_args()
print(args)

# ------------------------------------
# creamos la clase UnixUser
class UnixUser():
    """Clase UnixUser /etc/passwd login:passwd:uid:gid:gecos:shell:home"""
    def __init__(self,unixLine):
        unixField = unixLine.split(":")
        self.login = unixField[0]
        self.passwd = unixField[1]
        self.uid = int(unixField[2])
        self.gid = int(unixField[3])
        self.gname=""
        if self.gid in groupDict:
            self.gname=groupDict[self.gid].gname

    def show():
        pass

    def __str__(self):
        return "%s %s %d %d" % (self.login,self.passwd,self.uid,self.gid)

# ------------------------------------
# crea una clase UnixGroup

class UnixGroup():
    """Classe UnixGroup: prototipo de /etc/group gname:passwd:gid:listUsers"""
    def __init__(self,groupLine):
        groupField = groupLine.split(":")
        self.gname = groupField[0]
        self.passwd = groupField[1]
        self.gid = groupField[2]
        self.userListStr = groupField[3][:-1]
        self.userList = []
        if self.userListStr:
            self.userList = self.userListStr[:-1].split(",")

    def __str__(self):
        return "%s %d %s" % (self.gname, int(self.gid), self.userList)
# ------------------------------------

def sort_login(user):
    return user.login

def sort_gid(user):
    return (user.gid, user.login)

def sort_gname(user):
    return(user.gname, user.login)

# -------------------------------------
# Abrimos fichero de grupos
groupDict={}

gFile = open(args.fileG,"r")

# por cada linea 
for line in gFile:
    # creamos el objeto UnixGroup
    group = UnixGroup(line)
    # agreamos al dicc
    groupDict[group.gid] = group
gFile.close()

#--------------------------------------
# Abrimos el fichero de usuarios
uFile = open(args.fileU,"r")
userList=[]
# Por cada userline 
for line in uFile:
    # creamos el objeto UnixUser
    oneUser = UnixUser(line)
    # agregamos a la lista 
    userList.append(oneUser)
uFile.close()

# ordenamos segun criterio 
if args.criterio == "login":
    userList.sort(key=sort_login)
elif args.criterio == "gid": 
    userList.sort(key=sort_gid)
else: 
    userList.sort(key=sort_gname)

# listamos cada usuario de la lista ordenada
for user in userList:
    print(user, end="\n")

print(groupDict)
exit(0)
