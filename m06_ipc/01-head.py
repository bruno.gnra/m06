# /usr/bin/python
#-*- coding: utf-8-*-
# 
# head [file]
# 10 lines, file or stdin 
#----------------------------
# @edt ASIX M06 Curs 2022-2023
# Febrer 2023
#----------------------------
import sys

MAXLINE=10

fileIn = sys.stdin

# si el numero de argumentos >2 (se pasa un file) 
if len (sys.argv) == 2:
    # leemos el contenido del fichero
    fileIn = open(sys.argv[1],"r")

counter = 0
# por cada linea del fichero
for line in fileIn:
    # cont +1 
    counter += 1
    # mostramos la linea
    print(line,end="")
    # comparamos el numero de lineas printeadas
    if counter == MAXLINE:
        break
# cerramos fichero 
fileIn.close()

exit(0)


