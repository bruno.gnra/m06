# /usr/bin/python3
#-*- coding: utf-8-*- 

import sys,os,signal,argparse

parser = argparse.ArgumentParser(description="""Alarma""")
parser.add_argument("segons",type=int,help="Segundos")
args = parser.parse_args()

print(args)
#-----------------------------------------------------
up = 0 
down = 0

def myuser1(signum,frame):
    # llamamos a la variable global 
    global up
    print("Signal handler with signal:", signum)
    # seg actuales 
    actual = signal.alarm(0)
    print(actual)
    # + 60 seg  # plancho nuevamente la alarma + 60 seg
    signal.alarm(actual + 60)
    # cont +1 
    up+=1

def myuser2(signum,frame):
    # llamada a la variable global
    global down
    print("Signal handler with signal:", signum)
    # seg actuales @ojo que aqui cancelo la alarma
    actual = signal.alarm(0)
    # si es mayor a 60 
    if actual > 60:
        # -60 seg  # programa nuevamente la alarma con el valor anterior -60 
        signal.alarm(actual - 60)
    # si no 
    else: 
        # mostramos que no queda el tiempo suficiente
        print("ignored, not enough time", actual)
        # seg actuales # vuelvo a programar la alarma con el tiempo actual 
        signal.alarm(actual)
    down += 1

def myalarm(signum,frame):
    print("Signal handler with signal:", signum)
    # muesto las cantidades de up y down
    print("Finalizando... up: %d, down: %d" % (up,down))
    # exit 
    sys.exit(0)

def myterm (signum,frame):
    print("Signal handler with signal:", signum)
    # guardo los segundos actuales restantes, al ser 0 cancela la alarma
    falten = signal.alarm(0)
    # vuelvo a programar la alerta, con los segun restantes
    signal.alarm(falten)
    # muestro los segundos
    print("Quedan %d segunddos " % (falten))

def myhub(signum,frame):
    print("Signal handler with signal:", signum)
    # printamos los segundos programaos inicialmente 
    print("Reinciando alarma: ", args.segons)
    # Y reiniciamos la alarma 
    signal.alarm(args.segons)

# Asignamos un handler a la señal 
signal.signal(signal.SIGUSR1,myuser1) # 10
signal.signal(signal.SIGUSR2,myuser2) # 12
# signal.signal(signal.SIGINT,signal.SIG_IGN) # 2 
signal.signal(signal.SIGALRM,myalarm) #14
signal.signal(signal.SIGTERM,myterm) # 15
signal.signal(signal.SIGHUP, myhub) # 1 

signal.alarm(args.segons)
print(os.getpid())
while True:
    pass
signal.alarm(0)
sys.exit(0)
