# /usr/bin/python3 
#-*- coding: utf-8-*-

import sys, os, signal

def myusr1(signum,frame):
    print("Signal handler with signal:", signum)
    print("Hola Radiola")

def myuser2(signum,frame):
    print("Signal handler with signal:", signum)
    print("Adeu Andreu") 
    sys.exit(0)
#-----------------------------------------------
print("Hola comenzamos el programa pricipal")
print("PID pare: ", os.getpid())
# creamos un nuevo proceso 

pid = os.fork()

if pid != 0: 
    # os.wait()
    print("Programa padre:", os.getpid(), pid)
    print("Programa padre finalizado")
    sys.exit(0)

# Codigo del programa hijo. 
print("Programa hijo", os.getpid(), pid)
# Asignamos un handler a la señal 
signal.signal(signal.SIGUSR1,myusr1) # 10
signal.signal(signal.SIGUSR2,myuser2) # 12 

while True: 
    pass
print("Hasta luego Lucas")
sys.exit(0)