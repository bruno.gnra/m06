# /usr/bin/python3 
#-*- coding: utf-8-*-
#
# head-nulti.py [-n 5|10|15] [-f fileIn]...
# 
# @edt ASIX M06 CURS 2022-2023
# -----------------------------------------
import sys, argparse

parser = argparse.ArgumentParser(description="""Mostrar las N primeras lineas""", epilog="thats all floks")

parser.add_argument("-n","--nlin",type=int,dest="nlin",help="Numero de lineas",metavar="num",choices=[5,10,15],default=10)
parser.add_argument("-f","--file",type=str,dest="fileList",nargs="*",help="Fichero a procesar",metavar="file")
parser.add_argument("-v","--verbose",action="store_true",default=False)

args = parser.parse_args()
print(args)

# -----------------------------------------
# maximo de lineas a mostrar 
MAXLINES=args.nlin

# funcion para leer un fichero
def headFile(file):
    cont=0
    fileIn=open(file,"r")
    for line in fileIn: 
        cont += 1 
        print(line,end='')
        if cont == MAXLINES:
            break
    fileIn.close()

# por cada fichero de la lista
for fileName in args.fileList:
    # mostramos un encabezado si se indica verbose 
    if args.verbose:
        print("\n",fileName, 40*"-")
    # llamamos a la funcion headfile 
    headFile(fileName)

# fin 
exit(0)
