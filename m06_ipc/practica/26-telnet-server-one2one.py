# /usr/bin/python3 
#-*- coding: utf-8-*- 

import sys, socket, signal, os, argparse
from subprocess import Popen,PIPE

# ARGS 
parser = argparse.ArgumentParser("""Server Telnet one2one""")
parser.add_argument("-s","--server",type=str,help="Hostname o ip ",default='')
parser.add_argument("-p","--port",type=int,help="Port number", default=50001)
parser.add_argument("-d","--debug",action="store_true",help="trace de las acciones")
args = parser.parse_args()

# VARIABLES -----------------------------
HOST = args.server
PORT = args.port
DEBUG = args.debug
MYEOF = bytes(chr(4),'utf-8')
listpeers = []

# HANDLERS --------------------------------------------------------
def myusr1(signum,frame):
    print("Signal handler with signal:", signum)
    print(listpeers)
    sys.exit(0)

def myusr2(signum,frame):
    print("Signal handler with signal:", signum)
    print(len(listpeers))
    sys.exit(0)

def myterm(signum,frame):
    print("Signal handler with signal:",signum)
    print(listpeers,len(listpeers))
    sys.exit(0)

# CREAMOS EL SUBPROCESO ----------------------------------------
pid = os.fork()

if pid != 0:
    print("Programa padre {},{}".format(os.getpid(),pid))
    sys.exit(0)

# SIGNALS  ------------------------------------------------------
signal.signal(signal.SIGUSR1,myusr1) # 10
signal.signal(signal.SIGUSR2,myusr2) # 12
signal.signal(signal.SIGTERM,myterm) # 15
# PROGRAMA HIJO 
print("Programa hijo {},{}".format(os.getpid(),pid))
# CREAMOS LOS SOCKET TCP/IP
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
# INICIAMOS LA CONEXION  
s.bind((HOST,PORT))
# ESCUCHAMOS LAS CONEXIONES
s.listen(1)
print("Server Telnet escuchando por {}:{}".format(HOST,PORT))
print("Esperando por un nuevo cliente... ")

# Por cada cliente conectado 
while True:
    # aceptamos la conexion 
    conn, addr = s.accept()
    # agregamos el addr a lista de clientes conectados 
    listpeers.append(addr)
    # bucle para procesar la info recibida por el cliente 
    while True: 
        # comando 
        cmd = conn.recv(1024)
        if DEBUG:
            print("Recive", repr(cmd))
        # si no hay ordenes o se pasa un exit (cliente cierra la conexion)
        if not cmd or cmd.decode('utf-8')[:-1] == "exit": 
            break 
        # creo el popen
        pipeData = Popen(cmd, shell=True, stdout=PIPE,stderr=PIPE)
        # recorremos la info recibida por el cliente 
        for line in pipeData.stdout:
            conn.send(line)
            if DEBUG:
                sys.stderr.write(str(line,'utf-8'))
        # por cada linea de error
        for line in pipeData.stderr:
            #
            conn.send(line)
            if DEBUG:
                sys.stderr.write(str(line,'utf-8'))
        # yata 
        conn.sendall(MYEOF)
    conn.close()

# No acaba nunca, hay que para el servicio con systemctl stop o kill -15
s.close(0)
sys.exit(0)