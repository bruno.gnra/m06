# /usr/bin/python3 
#-*- coding: utf-8-*-
import sys,os,signal,argparse,socket
from subprocess import Popen,PIPE

# ARGS 
parser = argparse.ArgumentParser("""Server Telnet one2one""")
parser.add_argument("-s","--server",type=str,help="Hostname o ip ",default='')
parser.add_argument("-p","--port",type=int,help="Port number", default=50001)
args = parser.parse_args()
# VARIABLES -----------------------------------------
HOST = args.server
PORT = args.port
MYEOF = bytes(chr(4),'utf-8')

# CREAMOS EL SOCKET TCP/IP
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
# INICIAMOS LA CONEXION CON EL SERVER
s.connect((HOST,PORT))
# BUCLE PARA ENVIAR TODAS LAS ORDENES
while True:
    # obtenemos el dir activo
    pwd = os.getcwd()
    # ingresa la orden
    data = input("{}$ ".format(pwd))
    # enviamos la data al servidor 
    s.sendall(bytes(data,'utf-8'))
    if not data or str(data) == "exit":
        break
    # bucle para procesar la respuesta del server
    while True:
        data = s.recv(1024)
        # si se recibe un MYEOF = "yata"
        if data [-1:] == MYEOF:
            print(data[:-1].decode("utf-8")[:-1])
            break
        print(data.decode("utf-8")[:-1])
        # el [:-1] chapa el \n al final de la linea 
s.close()
sys.exit(0)


