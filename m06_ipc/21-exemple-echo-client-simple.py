# /usr/bin/python3
#-*- coding: utf-8-*- 
import sys, socket

HOST = "localhost"
PORT = 50001

# CREAMOS EL SOCKET TCP/IP
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

# iniciamos la conexión 
s.connect((HOST,PORT))
# enviamos la info al server 
s.send(b'Hello world')

# recibimos los datos del server
data = s.recv(1024)
s.close()
# retorna la representacion string de un objeto  
print("Recibido", repr(data))
sys.exit(0)
