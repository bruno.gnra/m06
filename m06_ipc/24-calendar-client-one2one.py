# /usr/bin/python3
#-*- coding: utf-8-*- 
import sys, socket, argparse
from subprocess import Popen,PIPE

HOST = ''
PORT = 50001

# CREAMOS EL SOCKET TCP/IP 
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
# INICIAMOS LA CONEXION 
s.connect((HOST,PORT))
# EN ESTE CASO EL CLIE NO ENVIA DATOS 
# ESPERAMOS RESPUESTA DEL SERVER 
while True:
    # leemos la info recibida 
    data = s.recv(1024)
    # si no se recibe mas info (server desconecta )
    if not data:
        break
    # mostramos los datos recibidos
    print(repr(data))
# cliente desconecta 
s.close()
sys.exit(0)
