# /usr/bin/python3 
#-*- coding: utf-8-*-
# 
# ------------------------------
# edt @ASIX M06 CURS 2022-2023
# Febrero 2023
# ------------------------------

import sys, argparse 

parser = argparse.ArgumentParser(description= """Lista todo los usuarios Unix""")
parser.add_argument("-f", type=str, dest="file",help="Fichero a procesar",metavar="file",default="/dev/stdin")
args = parser.parse_args()

# crea la clase 
class UnixUser():
    """Clase UnixUser: /etc/passwd login:passwd:id:gid:gecos:home:shell"""
    # constructor con los campos de /etc/passwd
    def __init__(self,userLine):
        # por cada linea separamos campos
        userField=userLine.split(":")
        self.login=userField[0]
        self.passwd=userField[1]
        self.uid=int(userField[2])
        self.gid=int(userField[3])
    # metodo show         
    def show(self):
        "Mostra les dades de l'usuari"
        print(f"login:{self.login} uid:{self.uid} gid:{self.gid}")
    # metodo mostrar 
    def __str__(self):
        return "%s %s %d %d" % (self.login, self.passwd, self.uid, self.gid)
 
#-------------------------------------------------

# abrimos el fichero 
fileIn=open(args.file,"r")
# lista de usuarios 
userList=[]
# por cada linea
for line in fileIn:
    # creamos el objeto 
    oneUser=UnixUser(line)
    #oneUser.show()
    # agregamos el usuario a la lista de usuarios
    userList.append(oneUser)
# cerramos fichero
fileIn.close()

# por cada user de la lista 
for user in userList:
    # mostramos el usuario
    print(user)
exit(0)
