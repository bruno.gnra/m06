# /usr/bin/python
#-*- coding: utf-8-*-
#
#
import argparse, sys

parser = argparse.ArgumentParser(description="""Mostrar las N primeras lineas""")

parser.add_argument("-f","--file",type=str,help="file a processar",metavar="file", default="/dev/stdin",dest="file")
parser.add_argument("-n","--nlin", type=int,dest="nlin",help="num de lineas", metavar="num",default=10) 

args=parser.parse_args()
print(args)
#--------------------
MAX = args.nlin
fileIn=open(args.file,"r")
cont=0 

for line in fileIn:
    cont += 1
    print(line,end="")    
    if cont==MAX: break
fileIn.close()

exit(0)




