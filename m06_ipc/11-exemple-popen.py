# /usr/bin/pytho3
#-*- coding: utf-8-*-
#
# --------------------------------
#
# edt @ASIX M06 CURS 2022-2023
# Febrero 2023
#
# --------------------------------
import sys, argparse
from subprocess import Popen,PIPE

parser = argparse.ArgumentParser(description="""Exemple Popen""")
parser.add_argument("ruta",type=str,help="directorio a listar") 
args = parser.parse_args()

#-------------------------------------
cmd = ["ls", args.ruta]
pipeData = Popen(cmd, stdout=PIPE)
for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")

exit(0)
