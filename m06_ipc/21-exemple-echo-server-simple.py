# /usr/bin/python3 
#-*- coding: utf-8-*-

import sys, socket

HOST = ''
PORT = 50001

# creamos el socket TCP/OP "s"
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
# enlazamos el socket a una interfaz y puerto especifico 
s.bind((HOST,PORT))
# escuchamos la conexiones entrantes 
s.listen(1)

print("Servidor escuchando en {}:{}".format(HOST,PORT))
print('Esperando por un nuevo cliente...')
# aceptamos la conexion 
conn, addr = s.accept()
print("Conexión por {}".format(addr))

# bucle para escuchar toda lo que el cliente envie
while True: 
    # Recibimos datos del cliente 
    data = conn.recv(1024)
    print('Datos recibidos del cliente:', data)
    # si el cliente plega 
    if not data:
        break
    # enviamos los datos al cliente (el envio es en paralelo, echo server)
    conn.send(data)
# cerramos la conexion 
conn.close()
sys.exit(0)