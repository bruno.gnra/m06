[SHH]
Verificacion host remoto
Autenticacion de usuario 
	password, publicKey, gssapi 
Autenticacion desastesa basada en host (deprecated)

	Al instalar ssh se genera automaticamente un par de claves privadas
		tipos:  DSA i RSA
	
	known_hosts - claves publicas de los hosts al que el usuario se ha conectado
	
	fingerprint - hash de la clave publica que se muestra en la primera conexión 
	
	authorized_keys - Contiene todas las claves publicas que tienen autorizacion para conectarse al host remoto sin password.
	
	id_rsa.pub	- clave publica de usuario (~/.ssh/)
	ssh_host_key.pub - clave publica del host (/etc/ssh/)
	
[Tipos de autenticacion]
	password - clasica 
	
	gssapi	- Autenticacion basada en Kerberos
	
	Publickey - Autenticacion atraves de las public keys (cal copia la clave publica dentro del del fichero authorized_keys del servidor remoto)
		
	Autenticacion per host: Acepta conexiones sin password de cualquier usuario proveniente de un trusted host. Host que tiene su clave publica en el authorized_keys del servidor remoto. 
	
	Man-in-the.middle Attack - cuando la clau publica del host remoto se modifica ( un otro host suplanta la identidad del host real), "StricMode yes" no permite conexiones si esto sucede. 
	
pr0yect02024# aws

[Autenticacion Publickey]
	ssh-keygen 
	scp localfile user@hostname:remotefile
	scp id_rsa.pub marta@debian:~/.ssh/authorized_keys
	ssh-copy-id user@hostremoto [agrega todas las claves publicas]
	
	ssh-copy-id -i /path/to/key.pub user@host | hostremoto [1 key en concreto]
	
[sshfs] - Nos permite montar un sistema de ficheros, basado en FUSE, a traves de una conexion SSH. 
	Se recomiendo habilitar el acceso por clau publica. Los archivos se crean en nombre de user remoto. 
	

	sudo apt-get install sshfs
	dpkg -L sshfs 
	/etc/ssh/ssh_config
	
	sshfs user@host:[dir] mountpoint [options]
	fusermount -u mountpoint , desmontar
[ssh-agent] - Gestor de claves para ssh, servicio en background indenpendiente. 
	 eval $(ssh-agent) -> echo $SSH_AUTH_SOCK 
	 
[X11 forwarding]
	/etc/ssh/ssh_config X11Forwarding yes
	
