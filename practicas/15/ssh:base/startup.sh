#! /bin/bash

#local users

cp ./ldap.conf /etc/ldap/
cp ./nslcd.conf /etc/
cp ./common-session /etc/pam.d/
cp /opt/docker/common-account /etc/pam.d/common-account
cp /opt/docker/common-auth /etc/pam.d/common-auth
cp ./pam_mount.conf.xml /etc/security/
cp ./nsswitch.conf /etc/

users="unix01 unix02 unix03 unix04 unix05"
for user in $users
do 
	useradd -m -s /bin/bash $user
	echo -e "$user\n$user" | passwd $user
done

# engegar serveis per fer el pam_ldap
/usr/sbin/nslcd
/usr/sbin/nscd

# crear homes ldapusers 
bash /opt/docker/ldapusers.sh

# engegar servei ssh
mkdir /run/sshd
/usr/sbin/sshd -D

