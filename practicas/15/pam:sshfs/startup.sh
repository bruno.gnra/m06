#! /bin/bash

#local users

cp ./ldap.conf /etc/ldap/
cp ./nslcd.conf /etc/
cp ./common-session /etc/pam.d/
#cp /opt/docker/common-account /etc/pam.d/common-account
#cp /opt/docker/common-auth /etc/pam.d/common-auth
cp ./pam_mount.conf.xml /etc/security/
cp ./nsswitch.conf /etc/
cp ./hosts /etc/hosts

users="unix01 unix02 unix03 unix04 unix05"
for user in $users
do 
	useradd -m -s /bin/bash $user
	echo -e "$user\n$user" | passwd $user
done

# engegar serveis per fer el pam_ldap
/usr/sbin/nslcd
/usr/sbin/nscd

# crear homes ldapusers 
bash /opt/docker/ldapusers.sh

mkdir /root/.ssh
chmod 700 /root/.ssh
#cp /opt/docker/known_hosts /root/.ssh/known_hosts
ssh-keyscan -p 2022 ssh.edt.org >> /root/.ssh/known_hosts

/bin/bash

